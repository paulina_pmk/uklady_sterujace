#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
uint8_t cyfra [] = {~0b00111111, ~0b00000110, ~0b01011011, ~0b01001111, ~0b01100110,
                ~0b01101101, ~0b01111101, ~0b0000111, ~0b01111111, ~0b01101111};

uint16_t time = 0;
uint8_t kolumna = 0;
int i=0;

uint8_t potegi [] = {1,10,100,1000};

ISR(TIMER0_COMP_vect){
    PORTB = ~(1<<kolumna); //to sprawi, że na odpowiedniej kolumnie będzie wyświetlana cyfra
    i = (time/(potegi[kolumna]))%10; // i to cyfra, którą chcemy wyświetlić
    PORTA = cyfra[i]; // na porcie A wyświetlamy wyliczoną cyfrę
    ++kolumna; //zaświecamy następną kolumnę
    kolumna &= 0x03; // zerujemy pozostałe
}

int main(void)
{
    DDRA = 0xFF;    //wyswietlacz 7-segmentowy ustawiamy na wyjscie
    DDRB = 0xFF;    // ustawiamy na wyjście; podłączone kolumny
    PORTB = 0xFE;    //wszystkie kolumny włączone
    PORTA = 0x00;    // ustawiamy na 0
    TCNT0 = 0;     //licznik wewnetrzny timera
    OCR0 = 250;  // wartość w czasie, w której będzie uruchomione przerwanie
    TIMSK |= (1 << OCIE0); //konfiguracja timera
    TCCR0 |= (1 << CS01) | (1 << CS00);    
   
    sei(); //globalne włączenie przerwań
    
    while(1)
    {
       time++;
       if (time >= 10000){ //po tym jak doliczymy do 9999 ustawiamy na 0
           time = 0;
       }
       _delay_ms(1000);
    }
    
}
