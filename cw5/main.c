#define F_CPU 16000000UL
#include "HD44780.h"
#define PIN_E PA5
#define PIN_RS PA4


void WriteNibble(unsigned char nibbleToWrite)
{
    //wyciagana jest zawsze najmlodsza czworka
    PORTA |= (1<<PIN_E); //ustawiamy E na 1 bez zmiany reszty bitow
    PORTA = (PORTA & 0xF0)| (nibbleToWrite & 0x0F); //zapis czterech bitow bez zmiany E
    PORTA &= ~(1<<PIN_E); //ustawiamy E na 0
}


void WriteByte(unsigned char dataToWrite)
{
    WriteNibble(dataToWrite>>4); //najpierw bierzemy 4 starsze 
    WriteNibble(dataToWrite);    //potem 4 mlodsze
    _delay_us(50);
}


void LCD_Command(unsigned char data)
{
    PORTA &= ~(1<<PIN_RS); //ustawiamy bit RS na 0
    WriteByte(data);
};

void LCD_Text(char * data)
{
    PORTA |= (1<<PIN_RS);
    while(*data){ //operator wyluskania(pobieranie wartosci pod danym adresem) - tak dlugo jak znak rozny od zera
        WriteByte(*data++);
    }
};

void LCD_GoToXY(unsigned char x, unsigned char y)
{
    LCD_Command(((y * 0x40) + x) | 0x80);
};

void LCD_Clear(void)
{
    //LCD_Command(1<<0);
};

void LCD_Home(void)
{
    //LCD_Command(1<<1);
};

void LCD_Initalize(void)
{
    _delay_ms(50);
    for(int i=0; i<3; i++){
        WriteNibble(3); //zapisujemy 0b0011****
        _delay_ms(5);
    }
    WriteNibble(2);
    _delay_ms(2);
    LCD_Command(0b00101000);
    _delay_ms(2);
    LCD_Command(0b00001000);
    _delay_ms(2);
    LCD_Command(0b00000001);
    _delay_ms(2);
    LCD_Command(0b00000110);
    _delay_ms(2);
    LCD_Command(0b00001111);
    _delay_ms(2);
    
};

int main()
{
    DDRA = 0xFF;    
    LCD_Initalize();
    LCD_GoToXY(0,0);
    LCD_Text("To dziala :)");
    while(1)
    {
        
        
    }
    
}
