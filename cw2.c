#define F_CPU 8000000UL // ustawione na 8 bo pracowalismy na Propoxach
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

uint16_t counter = 0;
ISR(INT0_vect){
    ++counter;
    PORTA = ~((PORTA & 0x80) | (counter & 0x7f)); // iloczyn z 0x80 zeby wziac stan tylko ostatniego bitu
                                            // iloczyn countera z 0x7f żeby wyzerować ósmy bit (uniknięcie nadpisania na 1)
                                            // ~ bo te plytki dzialaja "na odwrot"
}

void setup() //konfiguracja przerwania
{
    MCUCR |= (1 << ISC01); // ustawienie na spadek
    GICR |= (1 << INT0); // wlaczenie przerwania INT0
    sei(); // wlaczenie przerwan globalnie
}


int main(void)
{
    DDRA = 0xFF;    //ustawienie pinów sterujących jako wyjscia (diody)
    DDRD = 0x00;    //ustawienie pinów sterujących jako wejścia (przycisk)
   
    PORTA = 0x80;        //początkowe zapalenie diody
    PORTD |= (1 << PD2);    //włączenie rezystora podciągającego
    setup();
    while(1)
    {
        //miganie diody
        _delay_ms(1000);
        PORTA ^= 0x80;
       
    }
}
