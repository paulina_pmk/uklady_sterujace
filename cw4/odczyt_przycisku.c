#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include "keyboard.h"

unsigned int ktoryPrzycisk = 0;
int kolumna = 0;
int cyfra [] = {~0b00111111, ~0b00000110, ~0b01011011, ~0b01001111, ~0b01100110,
~0b01101101, ~0b01111101, ~0b0000111, ~0b01111111, ~0b01101111};

unsigned int i=0;
unsigned int j=0;

ISR(TIMER0_COMP_vect)
{
		ktoryPrzycisk = klawiatura();
		
		i = (ktoryPrzycisk % 10); //wartosc wyswietlana w kolumnie jednosci
		PORTD = 0b11111110;
		PORTA= cyfra[i];
		czekaj();
		j = (ktoryPrzycisk/10)%10; //wartosc wyswietlana w kolumnie dziesiatek
		PORTD = 0b11111101;
		PORTA = cyfra[j];
		czekaj();
}


int main(void){

	TCCR0 &= ~(1<< WGM00);			//ustawienie trybu CTC
	TCCR0 |=   (1<< WGM01);

	TCCR0 |= (1<< CS00) | (1<< CS01);	// ustawienie preskalera na 64
	TCCR0 &= ~(1<< CS02);

	TIMSK |= (1<< OCIE0);			//Konfiguracja timera
	OCR0 = 50;					// wartosc w czasie, w ktorej bedzie uruchomione przerwanie
	sei();					//globalne w�aczenie przerwan
	
					
	DDRA = 0xFF;
	PORTA = cyfra[0];
	DDRD = 0xFF;
	PORTD = 0x00;
	
	
	
	while(1){			
	}

}
