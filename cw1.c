/*
 * GccApplication1.c
 *
 * Created: 2015-11-05 08:48:41
 *  Author: Administrator
 */ 


#include <avr/io.h>

#define F_CPU 16000000UL
#include <util/delay.h>

int main(void)
{
	DDRA = 0xFF;
	DDRB = 0x00;
	PORTB = 0b00000001;
	
	while(1)
    {
	if (PINB & 0x01){
		//domyslne zachowanie gdy nic nie naciskamy
		PORTA = 0b00000000;
	}else{
		//gdy naciskamy przycisk
		for(int i=0; i<=8; i++){ //for odpowiedzialny za "przechodzenie" po diodach
			_delay_ms(300);
			PORTA = 1 << i;
		}
		
		for (int s=0; s<1000; s++){ // for odpowiedzialny za przygaszanie
			_delay_us(s); //s określa czas trwania wyświetlania i jego braku
			PORTA = 0xFF; //włączamy diody
			_delay_us(1000-s); //tu zmiejszamy okres świecenia więc światło wydaje się być słabsze
			PORTA = 0x00; //wyłączamy diody
		}
		
	} 
    }
}
